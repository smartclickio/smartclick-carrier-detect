<?php
/**
 * Created by PhpStorm.
 * User: onur
 * Date: 03.10.2015
 * Time: 19:52
 */

namespace SmartClick\CarrierDetect\Test;

use SmartClick\CarrierDetect\Country;

/**
 * Class CountryTest
 * @package SmartClick\CarrierDetect\Country\Test
 */
class CountryTest extends \PHPUnit_Framework_TestCase
{

    public function testCountryCodeCanBeLoadedFromYaml()
    {
        $country = new Country();
        $country->loadFromCountryCode("AA");

        $this->assertEquals("AA", $country->getCountryCode());
    }

    public function testCountryNameCanBeLoadedFromYaml()
    {
        $country = new Country();
        $country->loadFromCountryCode("AA");

        $this->assertEquals("Test Country", $country->getCountryName());
    }

    public function testCountryDescriptionCanBeLoadedFromYaml()
    {
        $country = new Country();
        $country->loadFromCountryCode("AA");

        $this->assertEquals("This is a test country", $country->getDescription());
    }

    public function testCountryStatusCanBeLoadedFromYaml()
    {
        $country = new Country();
        $country->loadFromCountryCode("AA");

        $this->assertEquals(1, $country->getStatus());
    }

    public function testCountryCommentsCanBeLoadedFromYaml()
    {
        $country = new Country();
        $country->loadFromCountryCode("AA");

        $this->assertEquals("This is a test comment", $country->getComments());
    }

    public function testCountryFixedLinesCanBeLoadedFromYaml()
    {
        $country = new Country();
        $country->loadFromCountryCode("AA");

        $this->assertCount(2, $country->getFixedLines());
        $this->assertEquals("Fixed Line 1", $country->getFixedLines()[0]);
        $this->assertEquals("Fixed Line 2", $country->getFixedLines()[1]);
    }
}

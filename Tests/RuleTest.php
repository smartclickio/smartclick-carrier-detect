<?php
/**
 * Created by PhpStorm.
 * User: onur
 * Date: 03.10.2015
 * Time: 23:45
 */

namespace SmartClick\CarrierDetect\Test;


use SmartClick\CarrierDetect\Country;

class RuleTest extends \PHPUnit_Framework_TestCase
{
    public function  testRuleCanBeLoaded()
    {
        $country = new Country();
        $country->loadFromCountryCode("AA");

        $mobile_operator = $country->getMobileOperators()[0];
        $rules = $mobile_operator->getRules();

        $this->assertEquals("TEST ISP", $rules[0]->isp);
        $this->assertEquals("TEST ORG", $rules[1]->org);
        $this->assertEquals("TEST BOTH ISP", $rules[2]->isp);
        $this->assertEquals("TEST BOTH ORG", $rules[2]->org);
        $this->assertEquals("TEST CONTAIN ISP", $rules[3]->contain_isp);
        $this->assertEquals("TEST CONTAIN ORG", $rules[4]->contain_org);
        $this->assertEquals("TEST CONTAIN BOTH ISP", $rules[5]->contain_isp);
        $this->assertEquals("TEST CONTAIN BOTH ORG", $rules[5]->contain_org);
    }

    public function  testRuleWithSpecialCharsCanBeLoaded()
    {
        $country = new Country();
        $country->loadFromCountryCode("AA");

        $mobile_operator = $country->getMobileOperators()[1];
        $rules = $mobile_operator->getRules();

        $this->assertEquals("é", $rules[0]->isp);
        $this->assertEquals("-'", $rules[1]->org);
    }

    public function  testRuleWithSpaceCanBeLoaded()
    {
        $country = new Country();
        $country->loadFromCountryCode("AA");

        $mobile_operator = $country->getMobileOperators()[1];
        $rules = $mobile_operator->getRules();

        $this->assertEquals("this is  a space example", $rules[2]->isp);
    }
}

<?php
/**
 * Created by PhpStorm.
 * User: onur
 * Date: 04.10.2015
 * Time: 11:08
 */

namespace SmartClick\CarrierDetect\Test;


use Symfony\Component\Yaml\Yaml;

class YamlFilesTest extends \PHPUnit_Framework_TestCase
{
    public function testEveryFileCanBeConvertedToArray()
    {
        $scanned_directory = array_diff(scandir(__DIR__ . "/../data/"), array('..', '.'));

        foreach ($scanned_directory as $file) {
            Yaml::parse(file_get_contents(__DIR__ . "/../data/" . $file));
        }
    }
}

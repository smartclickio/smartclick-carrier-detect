<?php
/**
 * Created by PhpStorm.
 * User: onur
 * Date: 03.10.2015
 * Time: 22:54
 */

namespace SmartClick\CarrierDetect\Test;


use SmartClick\CarrierDetect\Country;

class MobileOperatorTest extends \PHPUnit_Framework_TestCase
{
    public function testMobileOperatorName()
    {
        $country = new Country();
        $country->loadFromCountryCode("AA");

        $mobile_operator = $country->getMobileOperators()[0];

        $this->assertEquals("Test Normal", $mobile_operator->getName());
    }

    public function testMobileOperatorId()
    {
        $country = new Country();
        $country->loadFromCountryCode("AA");

        $mobile_operator = $country->getMobileOperators()[0];

        $this->assertEquals(0, $mobile_operator->getId());
    }

    public function testMobileOperatorRules()
    {
        $country = new Country();
        $country->loadFromCountryCode("AA");

        $mobile_operator = $country->getMobileOperators()[0];

        $this->assertCount(6, $mobile_operator->getRules());
    }
}

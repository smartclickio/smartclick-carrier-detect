<?php

namespace SmartClick\CarrierDetect;

class Result
{
    const RESULT_WIFI = 0;
    const RESULT_CARRIER_3G_FOUND = 1;
    const RESULT_COUNTRY_3G_FOUND = 2;

    private $country = null;
    private $mobile_operator = null;
    private $status = null;

    /**
     * Result constructor.
     * @param null   $country
     * @param null   $mobile_operator
     * @param $status
     */
    public function __construct($country, $mobile_operator, $status)
    {
        $this->country = $country;
        $this->mobile_operator = $mobile_operator;
        $this->status = $status;
    }

    /**
     * @return integer
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @return Country
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @return MobileOperator
     */
    public function getMobileOperator()
    {
        return $this->mobile_operator;
    }

}

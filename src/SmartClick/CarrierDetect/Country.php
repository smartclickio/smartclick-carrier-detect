<?php

namespace SmartClick\CarrierDetect;

use Symfony\Component\Yaml\Yaml;

class Country
{
    /**
     * @var  string country_code
     */
    private $country_code;

    /**
     * @var  string
     */
    private $country_name;

    /**
     * @var  string
     */
    private $description;

    /**
     * @var  integer
     */
    private $status;

    /**
     * @var  string
     */
    private $comments;

    /**
     * @var  string[]
     */
    private $fixed_lines;

    /**
     * @var  MobileOperator[]
     */
    private $mobile_operators = [];

    /**
     * @return string
     */
    public function getCountryCode()
    {
        return $this->country_code;
    }

    /**
     * @return string
     */
    public function getCountryName()
    {
        return $this->country_name;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @return string
     */
    public function getComments()
    {
        return $this->comments;
    }

    /**
     * @return String[]
     */
    public function getFixedLines()
    {
        return $this->fixed_lines;
    }

    /**
     * @return MobileOperator[]
     */
    public function getMobileOperators()
    {
        return $this->mobile_operators;
    }

    /**
     * @param string $country_code
     */
    public function setCountryCode($country_code)
    {
        $this->country_code = $country_code;
    }

    /**
     * @param string $country_name
     */
    public function setCountryName($country_name)
    {
        $this->country_name = $country_name;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @param int $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @param string $comments
     */
    public function setComments($comments)
    {
        $this->comments = $comments;
    }

    /**
     * @param string[] $fixed_lines
     */
    public function setFixedLines($fixed_lines)
    {
        $this->fixed_lines = $fixed_lines;
    }

    /**
     * @param MobileOperator[] $mobile_operators
     */
    public function setMobileOperators($mobile_operators)
    {
        $this->mobile_operators = $mobile_operators;
    }

    /**
     * @param $country_code
     * @throws \Exception
     */
    public function loadFromCountryCode($country_code)
    {
        $this->loadFromFile(__DIR__ . "/../../../data/" . $country_code . ".yaml");
    }

    /**
     * Loads data from yaml file
     * @param $file
     * @throws \Exception
     */
    public function loadFromFile($file)
    {
        $yaml_data = Yaml::parse($file);

        $this->country_code = $yaml_data['country_code'];
        $this->country_name = $yaml_data['country_name'];
        $this->description = $yaml_data['description'];
        $this->status = (int)$yaml_data['status'];
        $this->comments = $yaml_data['comments'];
        $this->fixed_lines = $yaml_data['fixed_lines'];

        foreach ($yaml_data['mobile_operators'] as $mobile_operator_arr) {
            $mobile_operator = new MobileOperator($mobile_operator_arr['id'], $mobile_operator_arr['name']);

            foreach ($mobile_operator_arr['mapping'] as $map) {
                $rule = new Rule();

                if (array_key_exists('isp', $map)) {
                    $rule->isp = $map['isp'];
                }
                if (array_key_exists('org', $map)) {
                    $rule->org = $map['org'];
                }
                if (array_key_exists('contain_isp', $map)) {
                    $rule->contain_isp = $map['contain_isp'];
                }
                if (array_key_exists('contain_org', $map)) {
                    $rule->contain_org = $map['contain_org'];
                }

                $mobile_operator->addRule($rule);

                unset($rule);
            }

            $this->mobile_operators[] = $mobile_operator;
        }
    }

    /**
     * Saves data into yaml file
     * @param $data
     * @return string
     */
    public function save($data)
    {
        return Yaml::dump($data);
    }

    public static function isExists($country_code)
    {
        return file_exists(__DIR__ . "/../../../data/" . $country_code . ".yaml");
    }
}
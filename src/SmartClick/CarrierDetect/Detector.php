<?php

namespace SmartClick\CarrierDetect;

class Detector
{
    public static function detect($country_code, $isp, $org)
    {
        $isp = trim($isp);
        $org = trim($org);

        $country = new Country();
        $country->loadFromCountryCode($country_code);

        foreach ($country->getMobileOperators() as $mobile_operator) {
            if ($mobile_operator->isMatch($isp, $org)) {
                return new Result($country, $mobile_operator, Result::RESULT_CARRIER_3G_FOUND);
            }
        }

        return new Result(null, null, Result::RESULT_WIFI);
    }

    public static function isExists($country_code)
    {
        return Country::isExists($country_code);
    }
}
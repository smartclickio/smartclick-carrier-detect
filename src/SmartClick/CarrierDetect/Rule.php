<?php

namespace SmartClick\CarrierDetect;

class Rule
{
    public $isp = null;
    public $org = null;

    public $contain_isp = null;
    public $contain_org = null;
}
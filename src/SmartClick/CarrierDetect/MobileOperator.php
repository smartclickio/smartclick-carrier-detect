<?php

namespace SmartClick\CarrierDetect;

class MobileOperator
{
    /**
     * @var  integer
     */
    private $id;

    /**
     * @var  string
     */
    private $name;

    /**
     * @var  Rule[]
     */
    private $rules = [];

    /**
     * @param $id integer
     * @param $name string
     */
    public function __construct($id, $name)
    {
        $this->id = $id;
        $this->name = $name;
    }

    /**
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return Rule[]
     */
    public function getRules()
    {
        return $this->rules;
    }

    /**
     * @param $rule Rule
     */
    public function addRule($rule)
    {
        $this->rules[] = $rule;
    }

    /**
     * Checks whether is matching or not
     * @param $isp String
     * @param $org String
     * @return bool Boolean
     */
    public function isMatch($isp, $org)
    {
        $result = false;

        foreach ($this->rules as $rule) {
            $control = 0;

            if ($rule->isp !== null) {
                $control = $isp == $rule->isp ? 1 : 2;
            }

            if ($control != 2 && $rule->org !== null) {
                $control = $org == $rule->org ? 1 : 2;
            }

            if ($control != 2 && $rule->contain_isp !== null) {
                $control = stristr($isp, $rule->contain_isp) !== false ? 1 : 2;
            }

            if ($control != 2 && $rule->contain_org !== null) {
                $control = stristr($org, $rule->contain_org) !== false ? 1 : 2;
            }

            if ($control == 1) {
                $result = true;
                break;
            }
        }

        return $result;
    }
}
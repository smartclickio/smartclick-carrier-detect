<?php
/**
 * Created by PhpStorm.
 * User: onur
 * Date: 04.10.2015
 * Time: 10:14
 */

namespace SmartClick\CarrierDetect\Test;


use SmartClick\CarrierDetect\Detector;
use SmartClick\CarrierDetect\Result;

class DetectorTest extends \PHPUnit_Framework_TestCase
{
    public function testCarrierCanBeDetected()
    {
        $this->assertEquals(Result::RESULT_CARRIER_3G_FOUND, Detector::detect("AA", "ésle ğuala ", "asd")->getStatus());
        $this->assertEquals("Test Detection", Detector::detect("AA", "ésle ğuala ", "asd")->getMobileOperator()->getName());

        $this->assertEquals(Result::RESULT_CARRIER_3G_FOUND, Detector::detect("AA", "TEST CONTAIN ISP ASD DAS ", "not matching of course")->getStatus());
        $this->assertEquals("Test Normal", Detector::detect("AA", "TEST CONTAIN ISP ASD DAS ", "not matching of course")->getMobileOperator()->getName());

        $this->assertEquals(Result::RESULT_CARRIER_3G_FOUND, Detector::detect("AA", "ğuala", "orğaç")->getStatus());
        $this->assertEquals("Test Detection", Detector::detect("AA", "ğuala", "orğaç")->getMobileOperator()->getName());

        $this->assertEquals(Result::RESULT_WIFI, Detector::detect("AA", "not matching of course ", "not matching of course")->getStatus());
    }
}
